# simple-spring-app

A Spring Boot application with Thymeleaf allows to save/delete/retrieve students from the database.

The project contains unit tests for the StudentService. Integration tests are not realized.

Also, when the application starts, the method from the ContentFileComposer component is launched, which finds all txt files in the project and writes the content from them to src/main/resources/generatedFile/txt.file

