package com.romanov.financePlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class FinancePlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinancePlatformApplication.class, args);
	}

}
