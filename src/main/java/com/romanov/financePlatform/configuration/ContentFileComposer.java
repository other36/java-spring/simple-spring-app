package com.romanov.financePlatform.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


@Component
@Slf4j
public class ContentFileComposer {
    private final String pathToFile = "src/main/resources/generatedFile/text.txt";

    /**
     *  Method annotated with PostConstruct, create text.txt file in directory "dir/",
     *  read all txt files in the project and write content to created file
     *
     * @throws IOException
     */
    @PostConstruct
    public void initFile() throws IOException {
        generateEmptyFile();
        String result = computeListOfStrings();
        writeToFile(result);
    }

    /**
     *  Write string content to file "text.txt" in "src/main/resources/generatedFile/"
     *
     * @param string content that should be wrote to file
     * @throws IOException
     */
    private void writeToFile(String string) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(pathToFile);
        byte[] strToBytes = string.getBytes();
        outputStream.write(strToBytes);

        outputStream.close();
    }

    /**
     *  Compute list of strings to one string due to StringBuilder
     *
     * @return string
     * @throws IOException
     */
    private String computeListOfStrings() throws IOException {
        List<String> strings = readAllTxtFiles();
        StringBuilder stringBuilder = new StringBuilder();
        strings.forEach(value -> {
            try {
                Files.readAllLines(Paths.get(value)).forEach(
                        line -> stringBuilder.append(line).append("\n")
                );
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        return stringBuilder.toString();
    }

    /**
     *  Get and sort all file paths, except "/build/*", and convert it to list of strings
     *
     * @return List of string paths
     * @throws IOException
     */
    private List<String> readAllTxtFiles() throws IOException {
        Path start = Paths.get(System.getProperty("user.dir"));
        List<String> paths = Files.walk(start, Integer.MAX_VALUE)
                .map(String::valueOf)
                .sorted()
                .collect(Collectors.toList());
        paths.removeIf(value -> !value.endsWith(".txt") || value.startsWith(start + "/build"));
        return paths;
    }

    /**
     *  Create empty text.txt file or clear existing in "src/main/resources/generatedFile/"
     *
     * @throws IOException
     */
    private void generateEmptyFile() throws IOException {
        File file = new File(pathToFile);
        file.getParentFile().mkdir();
        if (!file.createNewFile()) {
            new FileOutputStream(file.getAbsoluteFile()).close();
        }
    }
}
