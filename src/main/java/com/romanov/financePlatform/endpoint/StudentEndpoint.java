package com.romanov.financePlatform.endpoint;

import com.romanov.financePlatform.model.StudentInput;
import com.romanov.financePlatform.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping
@RequiredArgsConstructor
public class StudentEndpoint {
    private final StudentService service;

    /**
     *  Method take data of student and save new student to database if student does not exist
     *
     * @param student - data of student
     * @param model
     * @return String, redirection to home page
     */
    @PostMapping("/add")
    public String add(@Valid StudentInput student, Model model) {
        service.save(student);
        return "redirect:/";
    }

    /**
     *  Method take data of student and open html page for adding student
     *
     * @param student - data of student
     * @return String, name of html file: add-student
     */
    @GetMapping("/newStudent")
    public String newStudent(@ModelAttribute("student") StudentInput student) {
        return "add-student";
    }

    /**
     *  Method shows all student from database
     *
     * @param model
     * @return String, name of html file: index
     */
    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("students", service.getAll());
        return "index";
    }

    /**
     *  Delete student by id from database
     *
     * @param id - student's id for deleting
     * @return String, redirection to home page
     */
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id) {
        service.deleteById(id);
        return "redirect:/";
    }
}
