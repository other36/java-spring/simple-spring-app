package com.romanov.financePlatform.mapper;

import com.romanov.financePlatform.model.Student;
import com.romanov.financePlatform.model.StudentInput;
import com.romanov.financePlatform.model.StudentOutput;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class StudentMapper {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public Student toEntity(StudentInput input) {
        Student entity = new Student();
        entity.setName(input.getName());
        entity.setSurname(input.getSurname());
        entity.setPatronymic(input.getPatronymic());
        entity.setBirthday(LocalDate.parse(input.getDate(), formatter));
        entity.setGroup(input.getGroup());
        return entity;
    }

    public StudentOutput toOutput(Student entity) {
        return new StudentOutput(
                entity.getId(),
                entity.getName(),
                entity.getSurname(),
                entity.getPatronymic(),
                formatter.format(entity.getBirthday()),
                entity.getGroup()
        );
    }
}
