package com.romanov.financePlatform.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "students")
@Getter
@Setter
@ToString
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "student_name")
    private String name;
    @Column(name = "student_surname")
    private String surname;
    @Column(name = "student_patronymic")
    private String patronymic;
    @Column(name = "student_birthday")
    private LocalDate birthday;
    @Column(name = "student_group")
    private String group;
}
