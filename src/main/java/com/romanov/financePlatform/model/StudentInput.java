package com.romanov.financePlatform.model;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class StudentInput {
    @NotBlank
    String name;
    @NotBlank
    String surname;
    @NotBlank
    String patronymic;
    @NotBlank
    String date;
    @NotBlank
    String group;
}
