package com.romanov.financePlatform.model;

import lombok.Value;

@Value
public class StudentOutput {
    Integer id;
    String name;
    String surname;
    String patronymic;
    String date;
    String group;
}
