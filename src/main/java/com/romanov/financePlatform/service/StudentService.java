package com.romanov.financePlatform.service;

import com.romanov.financePlatform.exception.NotFoundException;
import com.romanov.financePlatform.mapper.StudentMapper;
import com.romanov.financePlatform.model.Student;
import com.romanov.financePlatform.model.StudentInput;
import com.romanov.financePlatform.model.StudentOutput;
import com.romanov.financePlatform.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository repository;
    private final StudentMapper mapper;

    public void save(StudentInput input) {
        Student student = mapper.toEntity(input);
        repository.save(student);
    }

    public List<StudentOutput> getAll() {
        return repository.findAll()
                .stream()
                .map(mapper::toOutput)
                .collect(Collectors.toList());
    }

    public Page<StudentOutput> getAll(Pageable pageable) {
        List<StudentOutput> students = repository.findAll()
                .stream()
                .map(mapper::toOutput)
                .collect(Collectors.toList());
        return new PageImpl<>(students, pageable, students.size());
    }

    public void deleteById(Integer id) {
        repository.findById(id).ifPresentOrElse(
                value -> repository.deleteById(id),
                () -> {throw new NotFoundException("Student with id " + id + "does not exists!");});
    }
}
