CREATE TABLE students (
    id IDENTITY PRIMARY KEY,
    student_name VARCHAR(45) NOT NULL,
    student_surname VARCHAR(45) NOT NULL,
    student_patronymic VARCHAR(45) NOT NULL,
    student_birthday DATE NOT NULL,
    student_group VARCHAR(5) NOT NULL
);