package com.romanov.financePlatform.service;

import com.romanov.financePlatform.exception.NotFoundException;
import com.romanov.financePlatform.mapper.StudentMapper;
import com.romanov.financePlatform.model.Student;
import com.romanov.financePlatform.model.StudentInput;
import com.romanov.financePlatform.model.StudentOutput;
import com.romanov.financePlatform.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class StudentServiceTest {
    @Mock
    private StudentRepository repository;
    @Mock
    private StudentMapper mapper;

    private StudentService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        service = new StudentService(repository, mapper);
    }

    @Test
    void shouldSaveStudent() {
        StudentInput input = new StudentInput(
                "name",
                "surname",
                "patronymic",
                "01/01/2000",
                "group"
        );
        Student student = new Student();
        Mockito.when(mapper.toEntity(input)).thenReturn(student);

        service.save(input);

        Mockito.verify(repository, Mockito.times(1)).save(student);
    }

    @Test
    void shouldGetAllStudents() {
        List<Student> students = new ArrayList<>();
        List<StudentOutput> studentOutputs = new ArrayList<>();
        StudentOutput firstOutput = new StudentOutput(
                1,
                "name1",
                "surname1",
                "patronymic1",
                "01/01/2000",
                "group1");
        StudentOutput secondOutput = new StudentOutput(
                2,
                "name2",
                "surname2",
                "patronymi2",
                "02/02/2000",
                "group2");
        Student firstStudent = new Student();
        Student secondStudent = new Student();
        studentOutputs.add(firstOutput);
        studentOutputs.add(secondOutput);
        students.add(firstStudent);
        students.add(secondStudent);
        Pageable pageable = PageRequest.of(0, 10);
        Mockito.when(repository.findAll()).thenReturn(students);
        Mockito.when(mapper.toOutput(firstStudent)).thenReturn(firstOutput);
        Mockito.when(mapper.toOutput(secondStudent)).thenReturn(secondOutput);

        Page<StudentOutput> expected = new PageImpl<>(studentOutputs, pageable, 2);
        Page<StudentOutput> actual = service.getAll(pageable);

        assertEquals(expected, actual);
    }

    @Test
    void shouldDeleteStudent() {
        Student student = new Student();
        Mockito.when(repository.findById(1)).thenReturn(Optional.of(student));

        service.deleteById(1);

        Mockito.verify(repository, Mockito.times(1)).deleteById(1);
    }

    @Test
    void shouldDeleteStudentWhenNotFound() {
        RuntimeException exception = assertThrows(NotFoundException.class,
                () -> service.deleteById(1));
        assertTrue(exception.getMessage().contains("Student with id " + 1 + "does not exists!"));
    }
}
